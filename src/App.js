import './App.css';
import ChatPage from "./containers/ChatPage";

function App() {
  return (
    <div className="App">
        <ChatPage/>
    </div>
  );
}

export default App;
