import React, {useEffect, useState} from 'react';
import SendMessage from "../components/SendMessage";
import Messages from "./Messages";
import axios from "axios";
import './ChatPage.css';

const url = 'http://146.185.154.90:8000/messages';
function ChatPage() {
    const [messages, setMessages] = useState([]);
    const [intrvl, setiIntrvl] = useState(0);

    function adjustPageRefresh() {
        const i = setInterval(() => {
            axios.get(url)
                .then(res => {
                    setMessages(res.data);
                })
        }, 3000);
        setiIntrvl(i);
    }

    useEffect(()=>{
        adjustPageRefresh();
    }, []);

    const sendMessage = async (author, message) => {
        clearInterval(intrvl);
        const data = new URLSearchParams();
        data.set('message', message);
        data.set('author', author);

        await axios.post(url, data);
        adjustPageRefresh();
    }

    return (
        <div className="ChatPage">
            <SendMessage
                onClick = {(auth, msg)=>sendMessage(auth, msg)}
            />
            <Messages
                key = 'Messages'
                messages = {messages}
                pageCount = {Math.ceil(messages.length / 5.0)}
            />
        </div>
    );
}

export default ChatPage;