import React, {useState} from 'react';
import Message from "../components/Message";
import {Pagination} from "@mui/material";


function Messages(props) {
    const [page, setPage] = useState(1);

    const onPageChanged = (event, value) => {
        setPage(value);
    }
    return (
        <div className='Messages'>
            {[...props.messages]
                .reverse()
                .slice((page-1)*5, page *5)
                .map(msg=>{
                    return (
                        <Message
                            key = {msg._id}
                            datetime =
                                {new Date(msg.datetime).toLocaleDateString() + ' '
                                + new Date(msg.datetime).toLocaleTimeString()}
                            author ={msg.author}
                            message ={msg.message}
                        />);
            })}

            <Pagination
                count = { props.pageCount }
                page = {page}
                onChange = {(e,v) => onPageChanged(e, v)}
                disabled={props.messages.length < 5}
                color="primary"
            />
        </div>
    );
}

export default Messages;