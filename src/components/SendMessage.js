import React, {useState} from 'react';
import {Button, TextField} from "@mui/material";

function SendMessage(props) {
    const [msg, setMsg] = useState('');
    const [author, setAuthor] = useState('');

    const handleSendingMessageSend = () => {
        console.log(author, msg)
        props.onClick(author, msg);
        setMsg('');
    }

    return (
        <div className='SendMessage'>
            <TextField
                id="outlined-basic"
                label="Author"
                variant="outlined"
                value={author}
                onChange={(e)=>setAuthor(e.target.value)}
            />
            <TextField
                id="outlined-basic"
                value={msg}
                label="Message"
                variant="outlined"
                onChange={(e)=>setMsg(e.target.value)}/>
            <Button
                variant="outlined"
                onClick={()=>handleSendingMessageSend()}
            >
             Send
            </Button>
        </div>
    );
}

export default SendMessage;