import React from 'react';
import {Card, CardContent, Typography} from "@mui/material";
import './Message.css';

function Message(props) {
    return (
            <Card sx={{ minWidth: 275 }}
                  className='Message'>
                <CardContent>
                    <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        className='MessageHeader'
                        gutterBottom>
                        <span className="AuthorName">{props.author}</span>
                        <span>{props.datetime}</span>
                    </Typography>
                    <Typography variant="h5" component="div">
                        {props.message}
                    </Typography>
                </CardContent>
            </Card>
    );
}

export default React.memo(Message);